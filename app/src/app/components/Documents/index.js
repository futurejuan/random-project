import React from 'react';
import DocsDetails from './DocsDetails';
import styles from './styles.scss';
import DocsSummary from './DocsSummary';
import PropTypes from 'prop-types';

const Documents = (props) => {
    return (
        <div className={styles.documents}>
            <DocsSummary docs={props.docs} />
            <br />
            <DocsDetails docs={props.docs} actions={props.actions}/>
        </div>
    )
}

Documents.propTypes = {
    docs: PropTypes.array.isRequired
}

export default Documents;