import React from 'react';
import styles from './styles.scss';
import PropTypes from 'prop-types';

const DocsSummary = (props) => {
    if (!props.docs) return null;
    const docsTotalSize = props.docs.reduce((sum, doc) => sum + doc.size, 0);

    return (
        <div className={styles.summaryRow}>
            <h1 className={styles.docsCount}>{props.docs.length} {`document${props.docs.length !== 1 ? 's' : ''}`}</h1>
            <h3 className={styles.docsSize}>Total Size: {docsTotalSize.toFixed(2)}kb</h3>
        </div>
    )
}

DocsSummary.propTypes = {
    docs: PropTypes.array.isRequired
}

export default DocsSummary;