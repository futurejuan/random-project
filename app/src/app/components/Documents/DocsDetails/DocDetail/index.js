import React from 'react';
import styles from './styles.scss';

const DocDetail = (props) => {
    if (!props.doc) return null;

    const { name, size } = props.doc;

    const nameToRender = name.length > 16 ? name.slice(0, 16).concat('...') : name;

    const handleDeleteClick = (event) => {
        event.preventDefault();
        props.actions.deleteImage(name);
    }

    return (
        <div className={styles.docDetail}>
            <h4 className={styles.docDetailTitle} title={name}>{nameToRender}</h4>
            <div className={styles.docDetailLowerRow}>
                <p>{size.toFixed(2)}kb</p>
                <button onClick={handleDeleteClick}>delete</button>
            </div>
        </div>
    )
}

export default DocDetail;