import React from 'react';
import DocDetail from './DocDetail';
import styles from './styles.scss';
import PropTypes from 'prop-types';

const DocsDetails = (props) => {
    if (!props.docs) return (
        <div className={styles.noDocsAvailable}>
            Looks like something went wrong. Please refresh the site or try again later.
        </div>
    );

    if (!props.docs.length) return (
        <div className={styles.noDocsAvailable}>
            Upload a file making click in the Upload button in the top right corner
        </div>
    );

    return (
        <div className={styles.docsDetails}>
            {
                props.docs.map((doc, index) => 
                    <DocDetail doc={doc} key={`${doc}-${index}`} actions={props.actions} />
                )
            }
        </div>
    )
}

DocsDetails.propTypes = {
    docs: PropTypes.array,
}

export default DocsDetails;