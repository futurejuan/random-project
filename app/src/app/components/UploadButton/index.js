import React from 'react';
import styles from './styles.scss';

const UploadButton = (props) => {
    let uploadFileInput = React.createRef();

    const triggerFileInput = () => {
        uploadFileInput.current.click();
    }

    const fileInputHandler = (event) => {
        const file = event.target.files.item(0);
        if (file.type.match(/\/(png|jpeg|jpg)$/gm)) {
            props.onUpload(file)
        }
    }

    return (
        <div className={styles.uploadButton}>
            <input 
                type="file" 
                name="image"
                accept="image/png, image/jpeg, image/jpg" 
                style={{display: 'none'}}
                ref={uploadFileInput}
                onChange={fileInputHandler} />
            <button type="button" onClick={triggerFileInput}>
                UPLOAD
            </button>
        </div>
    )
}

export default UploadButton;