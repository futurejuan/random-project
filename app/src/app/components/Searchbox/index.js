import React from 'react';
import debounce from 'lodash/debounce';
import styles from './styles.scss';
import PropTypes from 'prop-types';

class Searchbox extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.debounceSearch = debounce(this.searchImage, 500);
    }

    searchImage(event) {
        this.props.onSearch(event.target.value);
    }

    handleInputChange(event) {
        event.persist();
        return this.debounceSearch(event);
    }
    render() {
        return (
            <div className={styles.searchbox}>
                <input placeholder="Search documents..." onChange={this.handleInputChange} />
            </div>
        )
    }
}

Searchbox.propTypes = {
    onSearch: PropTypes.func.isRequired
}
export default Searchbox;