import React from 'react';
import Searchbox from './components/Searchbox';
import UploadButton from './components/UploadButton';
import Documents from './components/Documents';
const styles = require('./styles.scss');
import axios from 'axios';


class App extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            docs: []
        }
        this.searchImages = this.searchImages.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
    }

    componentDidMount() {
        this.searchImages();
    }

    searchImages(name) {
        axios.get('/api/images', { params: { name } })
            .then(res => {
                this.setState({
                    docs: res.data
                })
            })
    }

    uploadFile(imageFile) {
        let formData = new FormData();
        formData.append('image', imageFile);
        axios.post('/api/images', formData, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          })
          .then(() => this.searchImages());
    }

    deleteImage(imgName) {
        axios.delete(`/api/images/${imgName}`)
            .then(() => this.searchImages());
    }

    render() {
    
        return (
            <div className={styles.app}>
                <div className={styles.topRow}>
                    <Searchbox onSearch={this.searchImages} />
                    <UploadButton onUpload={this.uploadFile} />
                </div>
                <Documents docs={this.state.docs} actions={{ deleteImage: this.deleteImage}} />
            </div>
        )
    }
}

export default App;