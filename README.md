# Juan Ignacio Biltes - June 23, 2019

## Installation 
1- Install all dependencies:
> npm install

2- Run the app:
> npm start

That's it. Access the app in localhost:3000

### Some other scripts
- Analyze bundle
> npm run analyze

- Tests
> npm run test

- Dev mode
> npm run start-dev

## Security 
### Addressed
- XSS injection
- dnsPrefetchControl controls browser DNS prefetching
- hidePoweredBy to remove the X-Powered-By header
- All helmet defaults: https://helmetjs.github.io/

### *Not* been addressed 
- https
- cors
- restrictions on endpoints
- remove unused depoendencies (see improvements `Clean dependency tree`)

I ran out of time for these honestly. They're super important

## Improvements 
- **Tests**: App has almost no coverage. Improve both components tests, API tests and server.
- Brotli: It's almost there but I couldn't get it to work.
- Docker image & https: Add them to project.
- PropTypes: A lot of them are missing. Run `npm run lint` for specifics
- Security: Check the described not addressed items
- SSR: I couldn't get it to work due to css modules. It can be worked out but with due time.
- Error Handling: Some error cases weren't covered. E.g.: Fail on image upload 
- Clean dependency tree: I ran out of time, some dependencies weren't finally used so the tree needs some cleaning.
- Code Splitting: with webpack `optimization` we could move at least react and react-dom into a `vendor` asset
- Restrictions on uploads, we're not validating the extension of the file

## Libraries 
- Babel: Transpiling
- axios: I'm more comfortable with it due to it's error handling. I'd probably move to `fetch` to remove some bundle size.
- clean-webpack-plugin: remove dist dir on builds
- enzyme: Testing. I'm comfortable with it's interface altough it has some issues with hooks so it might be better to swtich to react-test-renderer
- eslint: code static check
- jest: testing. It's maintained by fb itself so it's the safest choice for testing react apps. Also, it comes with mocking included (instead of having to install sinon) and has snapshots which are great for code reviews
- helmet: for server security
- nodemon: server rebuild
- postcss/postcss-preset-env: maintaining backwards compat with css features
- esm: For using es6 modules on server. This needs some cross examination.
- lodash: Just for lodash/debounce. I've used it since it's easy to use and it's the solution I'm most familiar with. Watchout to not include all modules of lodash but only those necessaries


## API // Any general observation about the API? 

``` 
### GET /api/images 
// Description of the endpoint: 
// - what does the endpoint do? Finds all images under dir `api/images` 
// - what does it return? 
//   If everything went fine: Status: 200. Body: An array of images: [ { name: String, size: Number }, ... ]
// - does it accept specific parameters? No

### DELETE /images/:imgName
// Description of the endpoint: 
// - what does the endpoint do? Deletes an imgName 
// - what does it return? If everything went fine: Status: 200.
// - does it accept specific parameters? No

### POST /images
// Description of the endpoint: 
// - what does the endpoint do? Uploads an Image and stores it under `api/images` 
// - what does it return? If everything went fine: Status: 200. If something went wrong: Status 500 with due error.
// - does it accept specific parameters? Yes. Request must have multipart/form-data encoding.
``` 

## Other notes 
My apologies for the bad quality in testing. I focused on shipping the app since I had so little time and couldn't get to add most of the tests I wanted. \
On regular conditions I wouldn't ship an app in this state.
