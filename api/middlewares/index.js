const express = require('express');
const fs = require('fs');
const path = require('path');
const multer  = require('multer');

const imagesDir = path.resolve(__dirname, '../images');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, imagesDir)
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  });
 
  
const upload = multer({ storage })

const app = express();

const cache = new Map();

const imageResponse = (name, size) => ({ name, size});
app.get('/images', (req, res, next) => {
    const searchName = req.query.name;
    fs.readdir(imagesDir, (err, files) => {
        if (err) next(err);
        if (!files.length) res.send(files);

        res.locals.images = [];

        let desiredFiles = files.filter(fileName => {
            fileName = fileName.toLowerCase();
            const isValidFormat = !!fileName.match(/.(png|jpeg|jpg)$/gm);

            if (searchName) {
                return isValidFormat && fileName.includes(searchName.toLowerCase())
            }

            return isValidFormat;
        });

        desiredFiles.forEach(fileName => {
            if (cache.has(fileName)) return res.locals.images.push(imageResponse(fileName, cache.get(fileName)));

            fs.stat(path.resolve(imagesDir, fileName), (err, stats) => {
                const size = (stats.size/1024);
                cache.set(fileName, size);
                res.locals.images.push(imageResponse(fileName, cache.get(fileName)));
                if (res.locals.images.length === desiredFiles.length) {
                    res.send(res.locals.images);
                }
            })
        });

        if (res.locals.images.length === desiredFiles.length) {
            res.send(res.locals.images);
            next();
        }
    })
});

app.delete('/images/:imgName', (req, res, next) => {
    const fileName = req.params.imgName;
    if (!cache.has(fileName)) return res.status(404).send('Specified image does not exist');

    fs.unlink(path.resolve(imagesDir, fileName), (err) => {
        if (!err) return res.status(200).send();
        
        res.status(500).send(err);
    })
})

app.post('/images', upload.single('image'), function (req, res, next) {
    res.status(200).send();
})

module.exports = app;