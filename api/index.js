const express = require('express');
const config = require('./config');
const middlewares = require('./middlewares');

const app = express();

app.use('/', middlewares);

if (process.env.NODE_ENV === 'development') {
    app.listen(config.port, () => console.log(`API running on port ${config.port}`));
}

module.exports = app;